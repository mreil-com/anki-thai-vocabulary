package com.mreil.ankiconnectjava.util;

import com.google.common.reflect.ClassPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class ResourceReader {
    private final String rootDir;
    private final static Logger log = LoggerFactory.getLogger(ResourceReader.class);

    public ResourceReader(String rootDir) {
        this.rootDir = rootDir;
    }

    public List<ClassPath.ResourceInfo> getAllResources() throws IOException {
        ClassPath cp = ClassPath.from(this.getClass().getClassLoader());

        List<ClassPath.ResourceInfo> allResources = cp.getResources()
                .asList().stream()
                .filter(r -> r.getResourceName().startsWith(rootDir))
                .collect(Collectors.toList());
        return allResources;
    }

    public static List<ClassPath.ResourceInfo> getResources(Path directory) throws IOException {
        List<ClassPath.ResourceInfo> allResources = new ResourceReader(directory.toString()).getAllResources();
        if (allResources.isEmpty()) {
            log.error("Deck directory for Anki deck '{}' not found.", directory);
        }
        return allResources;
    }
}
