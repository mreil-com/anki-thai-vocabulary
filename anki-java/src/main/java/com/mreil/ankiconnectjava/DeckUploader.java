package com.mreil.ankiconnectjava;

import com.google.common.reflect.ClassPath;
import com.mreil.ankiconnectjava.data.DataLoader;
import com.mreil.ankiconnectjava.model.internal.DeckData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.mreil.ankiconnectjava.util.ResourceReader.getResources;

public class DeckUploader {
    private final Logger log = LoggerFactory.getLogger(DeckUploader.class);
    public static final String MODELS_DIR_NAME = "models";

    private final AnkiClient client;
    private final Path deckDirectory;
    private final DeckData deckData;
    private final Collection<Path> models;

    public DeckUploader(AnkiClient client, Path deckDirectory) throws IOException {
        this.client = client;
        this.deckDirectory = deckDirectory;
        this.deckData = DataLoader.loadData(deckDirectory);

        List<ClassPath.ResourceInfo> allResources = getResources(deckDirectory);

        Path modelsPath = deckDirectory.resolve(MODELS_DIR_NAME);
        models = allResources.stream()
                .map(res -> Paths.get(res.getResourceName()))
                .filter(path -> path.startsWith(modelsPath))
                .map(Path::getParent)
                .filter(path -> path.getParent().equals(modelsPath))
                .collect(Collectors.toSet());
        if (models.isEmpty()) {
            log.error("No models found at '{}'", modelsPath);
        }
    }

    public String upload() throws IOException {
        String deckName = deckDirectory.getFileName().toString();
        log.info("Uploading deck '{}'...", deckName);
        client.createDeck(deckName);

        for (Path model : models) {
            String modelName = new ModelUploader(client, model, deckData).upload();
            new DataUploader(deckName, client, modelName, deckData).upload();
        }

        return deckName;
    }
}
