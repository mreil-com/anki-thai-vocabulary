package com.mreil.ankiconnectjava;

import com.google.common.reflect.ClassPath;
import com.mreil.ankiconnectjava.model.ImmutableMediaFile;
import com.mreil.ankiconnectjava.model.MediaFile;
import com.mreil.ankiconnectjava.util.ResourceReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractAnkiJob implements AnkiJob {
    public static final String ANKI_DEFAULT_DIR = "anki";
    public static final String MEDIA_DIR_NAME = "media";
    public static final String DECKS_DIR_NAME = "decks";
    private final Logger log = LoggerFactory.getLogger(AbstractAnkiJob.class);
    private final String rootDir;
    private final Path mediaPath;
    private final List<Path> allMedia;
    private final Set<Path> decks;
    protected final String exportPath;

    @SuppressWarnings("UnstableApiUsage")
    public AbstractAnkiJob(String exportPath) throws IOException {
        this.exportPath = exportPath;

        rootDir = ANKI_DEFAULT_DIR;

        Path rootPath = Paths.get(rootDir);
        List<ClassPath.ResourceInfo> allResources = getResources();

        mediaPath = rootPath.resolve(MEDIA_DIR_NAME);
        allMedia = allResources.stream()
                .map(res -> Paths.get(res.getResourceName()))
                .filter(path -> path.startsWith(mediaPath.toString()))
                .filter(path -> !path.getFileName().startsWith(".")) // no hidden files
                .collect(Collectors.toList());
        if (allMedia.isEmpty()) {
            log.info("No media found at '{}'", mediaPath);
        }

        Path decksPath = rootPath.resolve(DECKS_DIR_NAME);
        log.info("Looking for decks at: {}", decksPath);
        decks = allResources.stream()
                .map(res -> Paths.get(res.getResourceName()))
                .filter(path -> path.startsWith(decksPath))
                .map(this::getParents)
                .flatMap(Collection::stream)
                .filter(path -> decksPath.equals(path.getParent()))
                .collect(Collectors.toSet());
        if (decks.isEmpty()) {
            log.error("No decks found at '{}'", decksPath);
        }
    }

    private Collection<Path> getParents(Path path) {
        Set<Path> parents = new HashSet<>();
        path = path.getParent();
        while (path != null && !path.equals(path.getRoot())) {
            parents.add(path);
            path = path.getParent();
        }
        return parents;
    }

    public List<ClassPath.ResourceInfo> getResources() throws IOException {
        List<ClassPath.ResourceInfo> allResources = new ResourceReader(rootDir).getAllResources();
        if (allResources.isEmpty()) {
            log.error("Root directory for anki resources '{}' not found.", rootDir);
        }
        return allResources;
    }

    public void run(AnkiClient client) {
        List<String> deckNames = decks.stream()
                .map(deck -> uploadDeck(client, deck))
                .collect(Collectors.toList());

        storeAllMediaFiles(client);

        deckNames.forEach(deckName -> exportDeck(client, deckName));
    }

    public void exportDeck(AnkiClient client, String deckName) {
        String outputPath = Paths.get(exportPath).resolve(deckName + ".apkg").toString();
        log.info("Exporting deck '{}' to '{}'", deckName, outputPath);
        client.export(deckName, outputPath);
    }

    public String uploadDeck(AnkiClient client, Path deck) {
        try {
            String deckName = new DeckUploader(client, deck).upload();
            // TODO
            client.setDefaultUnlimitedNewCardsAndRandomOrder();
            return deckName;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void storeAllMediaFiles(AnkiClient client) {
        // TODO cross-check

        Collection<MediaFile> allMediaFiles = allMedia.stream()
                .map(p -> ImmutableMediaFile.builder()
                        .name(p.getFileName().toString())
                        .base64Data(readData(p))
                        .build())
                .collect(Collectors.toList());
        client.storeMediaFiles(allMediaFiles);
    }

    private String readData(Path path) {
        try {
            ClassPathResource file = new ClassPathResource(path.toString());
            byte[] bytes = StreamUtils.copyToByteArray(file.getInputStream());
            return Base64.getEncoder().encodeToString(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
