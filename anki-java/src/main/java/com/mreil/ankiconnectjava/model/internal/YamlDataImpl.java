package com.mreil.ankiconnectjava.model.internal;

import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class YamlDataImpl extends AbstractDeckData {

    private static final Logger log = LoggerFactory.getLogger(YamlDataImpl.class);

    private YamlDataImpl(Collection<DataRecord> records,
                         Set<String> fieldNames) {
        super(records, fieldNames);
    }

    protected static DeckData fromFile(String file) throws IOException {
        Reader in = new FileReader(new ClassPathResource(file).getFile());
        Yaml yaml = new Yaml();
        List<Map<String, Object>> load = yaml.load(in);

        Set<String> allKeys = load.stream()
                .map(Map::keySet)
                .flatMap(Collection::stream)
                .collect(Collectors.toCollection(LinkedHashSet::new));

        List<DataRecord> records = load.stream()
                .map(m -> newRecord(allKeys, m))
                .collect(Collectors.toList());


        return new YamlDataImpl(records, allKeys);
    }

    private static DataRecord newRecord(Set<String> allKeys, Map<String, Object> m) {
        try {
            return new DataRecord(allKeys.stream()
                    .collect(Collectors.toMap(
                            key -> key,
                            e -> DataRecord.FieldValue.from(m.getOrDefault(e, ""))
                    )));
        } catch (Exception e) {
            log.error("Error creating record for entry: {}", m);
            throw e;
        }
    }

    private static Map<String, DataRecord.FieldValue> toMap(CSVRecord record) {
        return record.toMap().entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> new DataRecord.FieldValue(e.getValue())
                ));
    }
}
