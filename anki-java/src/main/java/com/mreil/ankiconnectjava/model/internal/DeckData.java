package com.mreil.ankiconnectjava.model.internal;

import com.mreil.ankiconnectjava.model.MediaFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public interface DeckData {
    static DeckData fromFile(String file) {
        try {
            if (file.matches("(?i).*\\.ya?ml")) {
                return fromYaml(file);
            } else if (file.matches("(?i).*\\.csv")) {
                return fromCsv(file);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static DeckData fromCsv(String file) throws IOException {
        return CsvDataImpl.fromFile(file);
    }

    static DeckData fromYaml(String file) throws IOException {
        return YamlDataImpl.fromFile(file);
    }

    static DeckData fromFiles(Set<String> dataFiles) {
        return new CombinedDeckDataImpl(dataFiles.stream()
                .map(DeckData::fromFile)
                .collect(Collectors.toList()));
    }

    Set<String> getFieldNames();

    Collection<DataRecord> getRecords();

    Collection<MediaFile> getAllMediaFiles(Path rootDir);
}
