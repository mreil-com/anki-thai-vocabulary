package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
@Value.Enclosing
@JsonDeserialize(builder = ImmutableCreateModelResponse.Builder.class)
public interface CreateModelResponse extends Response<CreateModelResponse.CreateModelResult> {

    @Value.Immutable
    @JsonDeserialize(builder = ImmutableCreateModelResponse.CreateModelResult.Builder.class)
    interface CreateModelResult {
        int getSortf();

        int getDid();

        String getLatexPre();

        String getLatexPost();

        long getMod();

        int getUsn();

        List<String> getVers();

        int getType();

        String getCss();

        String getName();

        List<Field> getFlds();

        List<Template> getTmpls();

        List<String> tags();

        String id();

        List<Object> req();
    }

    @Value.Immutable
    @JsonDeserialize(builder = ImmutableCreateModelResponse.Field.Builder.class)
    interface Field {
        String getName();

        int getOrd();

        boolean getSticky();

        boolean getRtl();

        String getFont();

        int getSize();

        List<String> getMedia();
    }

    @Value.Immutable
    @JsonDeserialize(builder = ImmutableCreateModelResponse.Template.Builder.class)
    interface Template {
        String name();

        int ord();

        String qfmt();

        String bafmt();

        String bqfmt();

        String afmt();

        Optional<Void> did();
    }
}
