package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

@Value.Immutable
@Value.Enclosing
public interface CreateDeckAction extends ParamsAction<CreateDeckAction.DeckParams> {

    @Override
    default String getAction() {
        return "createDeck";
    }

    @Value.Immutable
    interface DeckParams extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        String getDeck();
    }
}
