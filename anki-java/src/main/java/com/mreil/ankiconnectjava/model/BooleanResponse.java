package com.mreil.ankiconnectjava.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ImmutableBooleanResponse.Builder.class)
public interface BooleanResponse extends Response<Boolean> {
}
