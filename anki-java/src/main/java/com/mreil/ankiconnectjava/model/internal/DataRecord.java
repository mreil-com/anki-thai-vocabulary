package com.mreil.ankiconnectjava.model.internal;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// TODO make this a builder
public class DataRecord {
    public static final String TAGS = "tags";

    private final Map<String, FieldValue> fields;
    private final Set<String> tags;
    private final Set<String> mediaFileNames;

    public DataRecord(Map<String, FieldValue> fields) {
        // TODO add explicit tags set
        this.tags = new HashSet<>(fields
                .getOrDefault(TAGS, FieldValue.EMPTY)
                .cValue);
        this.mediaFileNames = fields.entrySet().stream()
                .filter(e -> e.getKey().startsWith("data_"))
                .flatMap(e -> e.getValue().cValue.stream())
                .collect(Collectors.toSet());
        this.fields = filter(fields);
    }

    public Map<String, FieldValue> getFields() {
        return fields;
    }

    public Set<String> getTags() {
        return tags;
    }

    public Set<String> getMediaFileNames() {
        return mediaFileNames;
    }

    private Map<String, FieldValue> filter(Map<String, FieldValue> fields) {
        fields.remove(TAGS);
        return fields.entrySet().stream()
                .map(this::filterAudio)
                .map(this::filterImage)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<String, FieldValue> filterAudio(Map.Entry<String, FieldValue> entry) {
        return filter(entry, "data_audio", this::newSoundEntry);
    }

    private Map.Entry<String, FieldValue> filterImage(Map.Entry<String, FieldValue> entry) {
        return filter(entry, "data_image", this::newImageEntry);
    }

    private Map.Entry<String, FieldValue> newSoundEntry(Map.Entry<String, FieldValue> e) {
        return new AbstractMap.SimpleEntry<>(e.getKey(),
                new FieldValue(e.getValue().stream()
                        .filter(v -> !v.isEmpty())
                        .map(v -> "[sound:" + v + "]")
                        .collect(Collectors.toList()))
        );
    }

    private Map.Entry<String, FieldValue> newImageEntry(Map.Entry<String, FieldValue> e) {
        return new AbstractMap.SimpleEntry<>(e.getKey(),
                new FieldValue(e.getValue().stream()
                        .filter(v -> !v.isEmpty())
                        .map(v -> "<img src=\"" + v + "\"/>")
                        .collect(Collectors.toList()))
        );
    }

    private Map.Entry<String, FieldValue> filter(Map.Entry<String, FieldValue> entry,
                                                 String key,
                                                 Function<Map.Entry<String, FieldValue>, Map.Entry<String, FieldValue>> replacement) {
        return Stream.of(entry)
                .filter(e -> e.getKey().equals(key))
                .filter(e -> !e.getValue().isEmpty())
                .map(replacement)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(entry);
    }

    public static class FieldValue {
        private final Collection<String> cValue;
        private final Map<String, String> mValue;
        private static final FieldValue EMPTY = new FieldValue(Collections.emptyList());

        public static FieldValue from(Object value) {
            Class<?> cl = value.getClass();
            if (cl.isAssignableFrom(String.class)) {
                return new FieldValue((String) value);
            } else if (Collection.class.isAssignableFrom(cl)) {
                return new FieldValue((Collection) value);
            } else if (Map.class.isAssignableFrom(cl)) {
                return new FieldValue((Map) value);
            }
            throw new IllegalArgumentException();
        }

        public FieldValue(String value) {
            this.cValue = ImmutableList.of(value);
            this.mValue = null;
        }

        public FieldValue(Collection<String> value) {
            this.cValue = value;
            this.mValue = null;
        }

        public FieldValue(Map<String, String> value) {
            this.cValue = null;
            this.mValue = value;
        }

        public boolean isEmpty() {
            return (mValue == null && cValue.isEmpty())
                    || (cValue == null && mValue.isEmpty());
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("cValue", cValue)
                    .add("mValue", mValue)
                    .toString();
        }

        public String toUploadString(String key) {
            if (cValue != null) {
                return listString(key);
            } else {
                return mapString(key);
            }
        }

        private String listString(String key) {
            if (cValue.isEmpty()) {
                return "";
            } else if (cValue.size() == 1) {
                return cValue.iterator().next();
            } else {
                return "<ul class='values-" + key + "'>" +
                        cValue.stream()
                                .map(v -> "<li>" + v + "</li>")
                                .collect(Collectors.joining())
                        + "</ul>";
            }
        }

        private String mapString(String key) {
            if (mValue.isEmpty()) {
                return "";
            } else {
                return "<ul class='values-" + key + "'>" +
                        mValue.entrySet().stream()
                                .map(e -> "<li><span class='li-k'>" + e.getKey() + "</span>: <span class='li-v'>" + e.getValue() + "</span></li>")
                                .collect(Collectors.joining())
                        + "</ul>";
            }
        }

        public Stream<String> stream() {
            return cValue.stream();
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("fields", fields)
                .add("tags", tags)
                .add("mediaFileNames", mediaFileNames)
                .toString();
    }
}
