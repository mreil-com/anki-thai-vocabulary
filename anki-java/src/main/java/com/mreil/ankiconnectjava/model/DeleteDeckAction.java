package com.mreil.ankiconnectjava.model;

import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Value.Enclosing
public interface DeleteDeckAction extends ParamsAction<DeleteDeckAction.DeckParams> {

    @Override
    default String getAction() {
        return "deleteDecks";
    }

    @Value.Immutable
    interface DeckParams extends DefaultParams {
        abstract class Builder implements ParamsBuilder {
        }

        List<String> getDecks();
    }
}
