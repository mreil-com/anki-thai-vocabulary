package com.mreil.ankiconnectjava.model.internal;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CombinedDeckDataImpl extends AbstractDeckData {
    protected CombinedDeckDataImpl(Collection<DeckData> collection) {
        super(records(collection), fieldNames(collection));
    }

    private static List<DataRecord> records(Collection<DeckData> collection) {
        return collection.stream()
                .map(DeckData::getRecords)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private static HashSet<String> fieldNames(Collection<DeckData> collection) {
        return collection.stream()
                .map(DeckData::getFieldNames)
                .flatMap(Collection::stream)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
