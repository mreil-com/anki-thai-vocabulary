package com.mreil.ankiconnectjava.model.internal;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.core.io.ClassPathResource;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CsvDataImpl extends AbstractDeckData {

    private CsvDataImpl(Collection<DataRecord> records,
                        Set<String> fieldNames) {
        super(records, fieldNames);
    }

    protected static DeckData fromFile(String file) throws IOException {
        Reader in = new FileReader(new ClassPathResource(file).getFile());
        CSVParser records = CSVFormat.RFC4180
                .withFirstRecordAsHeader()
                .withCommentMarker('#')
                .parse(in);

        List<DataRecord> dataRecords = records.getRecords().stream().map(record ->
                new DataRecord(toMap(record))
        ).collect(Collectors.toList());

        return new CsvDataImpl(dataRecords, new HashSet<>(records.getHeaderNames()));
    }

    private static Map<String, DataRecord.FieldValue> toMap(CSVRecord record) {
        return record.toMap().entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> new DataRecord.FieldValue(e.getValue())
                ));
    }
}
