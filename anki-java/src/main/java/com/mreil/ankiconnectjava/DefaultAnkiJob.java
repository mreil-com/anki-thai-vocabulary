package com.mreil.ankiconnectjava;

import com.google.common.base.MoreObjects;

import java.io.IOException;

public class DefaultAnkiJob extends AbstractAnkiJob {
    public DefaultAnkiJob(String exportPath) throws IOException {
        super(exportPath);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("exportPath", exportPath)
                .toString();
    }
}
