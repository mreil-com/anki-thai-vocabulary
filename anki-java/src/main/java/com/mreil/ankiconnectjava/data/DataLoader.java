package com.mreil.ankiconnectjava.data;

import com.google.common.reflect.ClassPath;
import com.mreil.ankiconnectjava.model.internal.DeckData;
import com.mreil.ankiconnectjava.util.ResourceReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

public class DataLoader {
    private static final String DATA_DIR = "data";
    private final static Logger log = LoggerFactory.getLogger(DataLoader.class);

    public static DeckData loadData(Path deckDirectory) throws IOException {
        Path dataDirectory = deckDirectory.resolve(DATA_DIR);
        Set<String> dataFiles = ResourceReader.getResources(dataDirectory).stream()
                .map(ClassPath.ResourceInfo::getResourceName)
                .filter(res -> res.matches(".*\\.(csv|ya?ml)"))
                .collect(Collectors.toSet());

        log.info("Found data files: {}", dataFiles);
        return DeckData.fromFiles(dataFiles);
    }
}
