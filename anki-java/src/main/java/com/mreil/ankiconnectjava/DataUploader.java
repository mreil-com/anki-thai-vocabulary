package com.mreil.ankiconnectjava;

import com.mreil.ankiconnectjava.model.AddNoteAction;
import com.mreil.ankiconnectjava.model.ImmutableAddNoteAction;
import com.mreil.ankiconnectjava.model.internal.DataRecord;
import com.mreil.ankiconnectjava.model.internal.DeckData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.stream.Collectors;

public class DataUploader {
    private final Logger log = LoggerFactory.getLogger(DataUploader.class);
    private final String deckName;
    private final AnkiClient client;
    private final String modelName;
    private final DeckData deckData;

    public DataUploader(String deckName, AnkiClient client, String modelName, DeckData deckData) {
        this.deckName = deckName;
        this.client = client;
        this.modelName = modelName;
        this.deckData = deckData;
    }

    public void upload() {
        for (DataRecord record : deckData.getRecords()) {
            addNote(deckName, modelName, client, record);
        }
    }

    private void addNote(String deckName, String modelName, AnkiClient client, DataRecord record) {
        log.info("Importing: " + record);
        ImmutableAddNoteAction.Note.Builder note = ImmutableAddNoteAction.Note.builder()
                .deckName(deckName)
                .modelName(modelName)
                .putAllFields(allFields(record))
                .addAllTags(record.getTags());

        AddNoteAction action = ImmutableAddNoteAction.builder()
                .params(ImmutableAddNoteAction.Params.builder()
                        .note(note.build())
                        .build())
                .build();
        client.addNote(action);
    }

    private Map<String, String> allFields(DataRecord record) {
        return record.getFields().entrySet().stream()
                .collect(Collectors.toMap(
                        e -> e.getKey(),
                        e -> e.getValue().toUploadString(e.getKey())
                ));
    }
}
