package com.mreil.ankiconnectjava;

import com.mreil.ankiconnectjava.runtime.AnkiRuntime;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;

public class AnkiRunner {
    private final Logger log = LoggerFactory.getLogger(AnkiRunner.class);

    public static void main(String[] args) throws IOException {
        new AnkiRunner().run();
    }

    public void run() throws IOException {
        Reflections reflections = new Reflections("");

        List<Class<? extends AnkiJobCollection>> implementations =
                reflections.getSubTypesOf(AnkiJobCollection.class).stream()
                        .filter(c -> !Modifier.isAbstract(c.getModifiers()))
                        .collect(Collectors.toList());

        implementations.forEach(cls -> log.info("Found anki job collections: {}", cls));

        submit(
                implementations.stream()
                        .map(this::newInstance)
                        .toArray(AnkiJobCollection[]::new)
        );
    }

    private AnkiJobCollection newInstance(Class<? extends AnkiJobCollection> cls) {
        try {
            return cls.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void submit(AnkiJobCollection... collections) throws IOException {
        AnkiRuntime runtime = AnkiRuntime.get();
        AnkiClient client = runtime.getClient();

        try {
            for (AnkiJobCollection collection : collections) {
                log.info("Running collection: {} ...", collection.getClass());
                runCollection(collection, client);
            }
        } finally {
            runtime.shutdown();
        }
    }

    private void runCollection(AnkiJobCollection collection, AnkiClient client) throws IOException {
        for (AnkiJob job : collection.getJobs()) {
            log.info("Running job: {} ...", job);
            job.run(client);
        }
    }
}
