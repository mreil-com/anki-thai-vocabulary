package com.mreil.ankidecks.thaialphabet;

import com.google.common.collect.Lists;
import com.mreil.ankiconnectjava.AnkiJob;
import com.mreil.ankiconnectjava.AnkiJobCollection;
import com.mreil.ankiconnectjava.DefaultAnkiJob;

import java.io.IOException;
import java.util.Collection;

public class AlphabetJob implements AnkiJobCollection {
    @Override
    public Collection<AnkiJob> getJobs() throws IOException {
        return Lists.newArrayList(
                new DefaultAnkiJob("/data")
        );
    }
}
